# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [Unreleased]

### Changed
- Updated to work with soc datacenter

## [2022-04-22]

### Added
- Tempo as an upstream service

## [2022-03-25]
### Changed
- Updated all links to point to new repository location
- Moved repository to [CarbonCollins - Cloud](https://gitlab.com/carboncollins-cloud) GitLab Group
