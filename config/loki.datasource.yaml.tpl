apiVersion: 1

datasources:
  - name: Loki
    type: loki
    access: proxy
    url: http://{{ env "NOMAD_UPSTREAM_ADDR_loki" }}
    jsonData:
      maxLines: 1000
