apiVersion: 1

datasources:
  # - name: Tempo
  #   type: tempo
  #   uid: EbPG8fYoz
  #   url: http://{{ env "NOMAD_UPSTREAM_ADDR_tempo" }}
  #   access: proxy
  #   basicAuth: false

  #   jsonData:
  #     tracesToLogsV2:
  #       # Field with an internal link pointing to a logs data source in Grafana.
  #       # datasourceUid value must match the uid value of the logs data source.
  #       datasourceUid: 'loki'
  #       spanStartTimeShift: '1h'
  #       spanEndTimeShift: '-1h'
  #       tags: ['job', 'instance', 'pod', 'namespace']
  #       filterByTraceID: false
  #       filterBySpanID: false
  #       customQuery: true
  #       query: 'method="${__span.tags.method}"'
  #     tracesToMetrics:
  #       datasourceUid: 'prom'
  #       spanStartTimeShift: '1h'
  #       spanEndTimeShift: '-1h'
  #       tags: [{ key: 'service.name', value: 'service' }, { key: 'job' }]
  #       queries:
  #         - name: 'Sample query'
  #           query: 'sum(rate(traces_spanmetrics_latency_bucket{$__tags}[5m]))'
  #     serviceMap:
  #       datasourceUid: 'prometheus'
  #     nodeGraph:
  #       enabled: true
  #     search:
  #       hide: false
  #     lokiSearch:
  #       datasourceUid: 'loki'
  #     spanBar:
  #       type: 'Tag'
  #       tag: 'http.path'
