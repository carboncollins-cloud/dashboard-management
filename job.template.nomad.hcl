job "dashboard-management" {
  name        = "Dashboard Management (Grafana)"
  type        = "service"
  region      = "se"
  datacenters = ["soc"]
  namespace   = "c3-monitoring"

  group "grafana" {
    count = 1

    consul {}

    volume "grafana-data" {
      type            = "csi"
      source          = "grafana-data"
      attachment_mode = "file-system"
      access_mode     = "single-node-writer"
      per_alloc       = true
    }

    network {
      mode = "bridge"
    }

    service {
      provider = "consul"
      name     = "grafana"
      port     = "3000"
      task     = "grafana"

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "loki"
              local_bind_port = 3100
            }

            // upstreams {
            //   destination_name = "tempo"
            //   local_bind_port = 3200
            // }

            // upstreams {
            //   destination_name = "mimir"
            //   local_bind_port = 9009
            // }

            // upstreams {
            //   destination_name = "influxdb"
            //   local_bind_port = 8086
            // }
          }
        }
      }

      check {
        expose = true
        name = "Application Health Status"
        type = "http"
        path = "/api/health"
        interval = "10s"
        timeout = "3s"
      }

      tags = [
        "internal-proxy.enable=true",
        "internal-proxy.consulcatalog.connect=true",
        "internal-proxy.http.routers.grafana.entrypoints=https",
        "internal-proxy.http.routers.grafana.rule=Host(`grafana.soc.carboncollins.se`)",
        "internal-proxy.http.routers.grafana.tls=true",
        "internal-proxy.http.routers.grafana.tls.certresolver=lets-encrypt",
        "internal-proxy.http.routers.grafana.tls.domains[0].main=*.soc.carboncollins.se"
      ]
    }

    task "grafana" {
      driver = "docker"
      user = "[[ .defaultUserId ]]"

      config {
        image = "[[ .grafanaImage ]]"
      }

      volume_mount {
        volume = "grafana-data"
        destination = "/var/lib/grafana"
        read_only = false
      }

      vault {
        role = "service-grafana"
      }

      template {
        data = <<EOH
[[ fileContents "./config/grafana.ini.tpl" ]]
        EOH

        destination = "secrets/grafana.ini"
      }

      template {
        data = <<EOH
[[ fileContents "./config/loki.datasource.yaml.tpl" ]]
        EOH
        destination = "local/provisioning/datasources/loki.datasource.yaml"
      }

//       template {
//         data = <<EOH
// [ [ fileContents "./config/tempo.datasource.yaml.tpl" ]]
//         EOH
//         destination = "local/provisioning/datasources/tempo.datasource.yaml"
//       }

//       template {
//         data = <<EOH
// [ [ fileContents "./config/mimir.datasource.yaml.tpl" ]]
//         EOH
//         destination = "local/provisioning/datasources/mimir.datasource.yaml"
//       }

      env {
        TZ = "[[ .defaultTimezone ]]"

        GF_PATHS_CONFIG= "${NOMAD_SECRETS_DIR}/grafana.ini"
        GF_PATHS_DATA = "/var/lib/grafana"
        GF_PATHS_PROVISIONING = "${NOMAD_TASK_DIR}/provisioning"
      }
    }
  }

  reschedule {
    delay          = "10s"
    delay_function = "exponential"
    max_delay      = "10m"
    unlimited      = true
  }

  update {
    health_check      = "checks"
    min_healthy_time  = "10s"
    healthy_deadline  = "10m"
    progress_deadline = "15m"
    auto_revert       = true
  }

  meta {
    gitSha      = "[[ .gitSha ]]"
    gitBranch   = "[[ .gitBranch ]]"
    pipelineId  = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId   = "[[ .projectId ]]"
    projectUrl  = "[[ .projectUrl ]]"
    statefull   = "true"
  }
}
