terraform {
  backend "consul" {
    path = "terraform/monitoring-dashboard-management"
  }

  required_providers {
    nomad = {
      source = "hashicorp/nomad"
      version = "2.1.0"
    }
  }
}

provider "nomad" {}

module "soc_volumes" {
  source = "../../modules/grafanaNomadVolumes"

  plugin_id = "soc-axion-smb"

  cifs_user_id = 3205
  cifs_group_id = 3205
}
